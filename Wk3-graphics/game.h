// Game.h

#ifndef _GAME_H		//prevent multiple definitions of this class
#define _GAME_H

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <Mmsystem.h>
#include "graphics.h"
#include "input.h"
#include "constants.h"
#include "gameError.h"
#include "console.h"
#include "textDX.h"

namespace gameNS
{
	const char FONT[] = "Courier New"; // Console Font
	const int POINT_SIZE = 14;
	const COLOR_ARGB FONT_COLOR = SETCOLOR_ARGB ( 255,255,255,255 ); // White
}

class Game 
{
protected:
	// common game properties
	Graphics* graphics;			// pointer to Graphics
	Input* input;				// pointer to Input;
	Console* console;
	HWND	hwnd;				// window handle
	HRESULT	hr;					// return type from Windows/DirectX
	LARGE_INTEGER timeStart;	// performance counter start value
	LARGE_INTEGER timeEnd;		// performance counter end value
	LARGE_INTEGER timerFreq;	// performance counter frequency
	float frameTime;			// time required for last frame
	float fps;					// frames per second
	DWORD sleepTime;			// number of milliseconds to sleep between frames
	bool paused;				// true if game paused
	bool initialized;			// true if initialized

	bool fpsOn;					// True when displaying
	TextDX dxFont;
	std::string command;

public:
	// constructor
	Game();
	// destructor
	virtual ~Game();

	// *** Member Functions ***
	
	// window message handler
	LRESULT messageHandler( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam );

	// initialize the game
	virtual void initialize( HWND hwnd ); 

	// call run repeatedly by the main message loop in WinMain
	virtual void run( HWND hwnd );

	// call when the graphics device is lost
	virtual void releaseAll();

	// recreate all surfaces and reset all entities
	virtual void resetAll();

	// delete all reserved memory
	virtual void deleteAll();

	// render game items
	virtual void renderGame();

	// handle lost graphics device
	virtual void handleLostGraphicsDevice();

	// return pointer to graphics
	Graphics* getGraphics() { return graphics; }

	// return pointer to input
	Input* getInput() { return input; }

	// exit the game
	void exitGame() { PostMessage( hwnd, WM_DESTROY, 0, 0 ); }\

	// Process console commands
	virtual void consoleCommand();

	// ** pure virtual function declarations **
	// update game items
	virtual void update() = 0;

	// perform AI calculations
	virtual void ai() = 0;
	
	// check for collisions
	virtual void collisions() = 0;

	// render graphics
	virtual void render() = 0;

};

#endif