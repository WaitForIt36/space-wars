#ifndef _SHIP_H_
#define _SHIP_H_
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace shipNS
{
	// Related to the spritesheet
	const int WIDTH					 = 60;					// Width of the ship image
	const int HEIGHT				 = 60;					// Height of the ship image
	const int X = GAME_WIDTH/2 - WIDTH/2;
	const int Y = GAME_HEIGHT/2 -HEIGHT/2;

	const float SPEED				 = 300.0f;
	const float MASS				 = 300.0f;
	const float ROTATION_RATE		 = (float)PI/4;

	// Ship related constants
	const int TEXTURE_COLS			 = 8;
	const int SHIP1_START_FRAME		 = 0;					// Starting frame of animation
	const int SHIP1_END_FRAME		 = 3;					// Ending frame of animation
	const int SHIP2_START_FRAME		 = 8;					// Starting frame of animation
	const int SHIP2_END_FRAME		 = 11;					// Ending frame of animation

	const float SHIP_ANIMATION_DELAY = 0.2f;				// Time between frames of ship animation

	const int SHIELD_START_FRAME	 = 24;
	const int SHIELD_END_FRAME		 = 27;
	const float SHIELD_ANIMATION_DELAY = 0.2f;
}

class Ship : public Entity
{
private:
	bool shieldOn;	// Store if the shield id on
	Image shield;	//

public:
	Ship();

	void update( float frameTime );

	virtual void draw();
	virtual bool initialize( Game* gamePtr, int width, int height, int ncols, TextureManager* texturePtr );
	void damage( WEAPON weapon );

};


# endif