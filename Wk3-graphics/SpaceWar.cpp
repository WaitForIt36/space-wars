#include "spacewar.h"

Spacewar::Spacewar()
{}

Spacewar::~Spacewar()
{
	releaseAll();
}

// initialize the game
void Spacewar::initialize( HWND hwnd )
{
	Game::initialize( hwnd ); // call game's initialize
	// initialize the textures
	if( !deathStarTexture.initialize( graphics, DEATH_STAR_IMAGE ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing DeathStar Image" ) );
	}

	if( !lazerTexture.initialize( graphics, LAZER_IMAGE ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing Lazer Image" ) );
	}

	if( !bgTexture.initialize( graphics, BG_IMAGE ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing Background Image" ) );
	}

	if( !mfTextures.initialize( graphics, MF_IMAGE ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing MF Image" ) );
	}

	// initialize the Images
	if( !deathStar.initialize( this, 0, 0, 0, &deathStarTexture ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing Deathstar Texture" ) );
	}
	// Place the DeathStar in the center of the screen 
	deathStar.setX( GAME_WIDTH/**0.5f*/ - deathStar.getWidth()/**0.5f*/ ); 
	deathStar.setY( 0/*GAME_HEIGHT*0.5f - deathStar.getHeight()*0.5f*/ );

	if( !bg.initialize( graphics, 0, 0, 0, &bgTexture ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing bg Texture" ) );
	}
	bg.setX(0);
	bg.setY(0);

	if( !ship1.initialize( this, 0, 0, 0, &mfTextures ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing ship Texture" ) );
	}
	// Starting Ship postion on screen
	ship1.setX( GAME_WIDTH*1/2 );
	ship1.setY( GAME_HEIGHT*3/4 );
	ship1.setVelocity( VECTOR2( 0, -shipNS::SPEED ) );
	ship1.setDegrees( 0.0f );

	if( !lazer.initialize( graphics, 0, 0, 0, &lazerTexture ) )
	{
		throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing Lazer Texture" ) );
	}
	
	// Turns on the animation (lets image know this image is animated )
	//ship1.setFrames( shipNS::SHIP1_START_FRAME, 0 );								// Set up the animation frames
	//ship1.setCurrentFrame( shipNS::SHIP1_START_FRAME );							// Starting frame
	//ship1.setFrameDelay( shipNS::SHIP_ANIMATION_DELAY );							// How long to show each frame

	//if( !ship2.initialize( this, shipNS::WIDTH, shipNS::HEIGHT, shipNS::TEXTURE_COLS, &gameTextures ) )
	//{
	//	throw( GameError(gameErrorNS::FATAL_ERROR, "Error initializing ship image" ) );
	//}

	//// Starting Ship postion on screen
	//ship2.setX( GAME_WIDTH/4 );
	//ship2.setY( GAME_HEIGHT/4 );
	//
	//// Turns on the animation (lets image know this image is animated )
	//ship2.setFrames( shipNS::SHIP2_START_FRAME, shipNS::SHIP2_END_FRAME );		// Set up the animation frames
	//ship2.setCurrentFrame( shipNS::SHIP2_START_FRAME );							// Starting frame
	//ship2.setFrameDelay( shipNS::SHIP_ANIMATION_DELAY );							// How long to show each frame

	//ship2.setVelocity( VECTOR2( 0, -shipNS::SPEED ) );

	//ship2.setDegrees( 0.0f );

	return;
}

void Spacewar::update()
{
	bg.update(frameTime);
	//ship1.gravityForce( &DeathStar, frameTime );
	//ship2.gravityForce( &DeathStar, frameTime );

	// Anaimate the ship
	ship1.update( frameTime );
	deathStar.update(frameTime);

	/* ship2.update( frameTime );*/

	// Rotate the ship
	// ship1.setDegrees( ship1.getDegrees() + frameTime * 100.0f );

	// Scale the ship
	// ship1.setScale( ship1.getScale() - frameTime * 0.0f );

	// Movement
	// ship1.setX( ship1.getX() + frameTime * 100.0f );

	if( ship1.getX() > GAME_WIDTH )	
	{
		ship1.setX ( (float)-ship1.getWidth() );
		ship1.setScale( 1.5f );
	}
	if( input->isKeyDown( SHIP_RIGHT_KEY ) )
	{
		if( ship1.getX() < ( GAME_WIDTH - ship1.getWidth() ) )
			ship1.setX( ship1.getX() + frameTime * shipNS::SPEED );
		ship1.setDegrees( 0 );
	}
	if( input->isKeyDown( SHIP_LEFT_KEY ) )
	{
		if ( ship1.getX() > 0 )
			ship1.setX( ship1.getX() - frameTime * shipNS::SPEED );
		ship1.setDegrees( 0 );
	}
	if( input->isKeyDown( SHIP_UP_KEY ) )
	{
		if ( ship1.getY() > 0 )
			ship1.setY( ship1.getY() - frameTime * shipNS::SPEED );
		ship1.setDegrees( 0 );
	}
	if( input->isKeyDown( SHIP_DOWN_KEY ) )
	{
		if ( ship1.getY() < ( GAME_HEIGHT - ship1.getHeight() )  )
			ship1.setY( ship1.getY() + frameTime * shipNS::SPEED );
		ship1.setDegrees( 0 );
	}
	if( input->isKeyDown( S_KEY ) )
	{
		if ( ship1.getX() > 0 )
			ship1.setX( ship1.getX() - frameTime * shipNS::SPEED );
	}

}

void Spacewar::ai()
{}

void Spacewar::collisions()
{
	VECTOR2 collisionVector;
	if( ship1.collidesWith( deathStar, collisionVector ) )
	{
		// Bonce of the DeathStar
		ship1.bounce( collisionVector, deathStar );
		ship1.damage( DEATHSTAR );
	}

	//if( ship2.collidesWith( black, collisionVector ) )
	//{
	//	// Bonce of the DeathStar
	//	ship1.bounce( collisionVector, black );
	//	ship1.damage( DEATHSTAR );
	//}

	//if( ship1.collidesWith( ship2, collisionVector ) )
	//{
	//	ship1.bounce( collisionVector, ship2 );
	//	ship1.damage( SHIP );

	//	ship2.bounce( collisionVector * -1, ship1 );
	//	ship2.damage( SHIP );
	//}
}

void Spacewar::render()
{ 
	graphics->spriteBegin();
	// call draw functions after!  ORDER IS IMPORTANT!
	bg.draw();
	//nebula.draw();
	deathStar.draw();
	ship1.draw();
	//ship2.draw();
	
	//Call draw functions before!
	graphics->spriteEnd();
}

void Spacewar::releaseAll()
{
	gameTextures.onLostDevice();
	bgTexture.onLostDevice();
	deathStarTexture.onLostDevice();
	Game::releaseAll();
}

void Spacewar::resetAll()
{
	gameTextures.onResetDevice();
	bgTexture.onResetDevice();
	deathStarTexture.onResetDevice();
	Game::resetAll();
}
