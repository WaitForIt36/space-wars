#ifndef _SPACEWAR_H
#define _SPACEWAR_H
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "entity.h"
#include "DeathStar.h"
#include "Ship.h"

class Spacewar : public Game
{
private:
	// Variables
	TextureManager	bgTexture;			// Background Texture
	TextureManager	deathStarTexture;	// DeathStar Texture 
	TextureManager	gameTextures;
	TextureManager	mfTextures;
	TextureManager	lazerTexture;
 
	Image			bg;
	Image			lazer;
	DeathStar		deathStar;			// DeathStar image
	Ship			ship1;
	Ship			ship2;

public:
	Spacewar();							// Constructor
	virtual ~Spacewar();				// Destructor

	void initialize( HWND hwnd );

	void update();						// Override pure virtual from Game
	void ai();							// Override pure virtual from Game
	void collisions();					// Override pure virtual from Game
	void render();						// Override pure virtual from Game

	void releaseAll();
	void resetAll();
};

#endif
