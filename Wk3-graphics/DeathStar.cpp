// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 version 1.0

#include "DeathStar.h"

//=============================================================================
// default constructor
//=============================================================================
DeathStar::DeathStar() : Entity()
{
    spriteData.x    = DeathStarNS::X;              // location on screen
    spriteData.y    = DeathStarNS::Y;
    radius          = DeathStarNS::COLLISION_RADIUS;
    mass            = DeathStarNS::MASS;
    startFrame      = DeathStarNS::START_FRAME;    // first frame of ship animation
    endFrame        = DeathStarNS::END_FRAME;      // last frame of ship animation
    setCurrentFrame(startFrame);
}